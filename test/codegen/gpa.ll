; ModuleID = 'quack llvm'
source_filename = "quack llvm"

@0 = private unnamed_addr constant [12 x i8] c"value = %d\0A\00", align 1
@1 = private unnamed_addr constant [12 x i8] c"value = %d\0A\00", align 1

declare i32 @printf(i8*, ...)

define i32 @PLUS(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %addtmp = add i32 %x_load, %y_load
  ret i32 %addtmp
}

define i32 @MINUS(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %minus_tmp = sub i32 %x_load, %y_load
  ret i32 %minus_tmp
}

define i32 @TIMES(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %multmp = mul i32 %x_load, %y_load
  ret i32 %multmp
}

define i32 @DIVIDE(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %divtmp = sdiv i32 %x_load, %y_load
  ret i32 %divtmp
}

define i1 @ATMOST(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %cmptmp = icmp ule i32 %x_load, %y_load
  ret i1 %cmptmp
}

define i1 @ATLEAST(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %cmptmp = icmp uge i32 %x_load, %y_load
  ret i1 %cmptmp
}

define i1 @LESS(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %cmptmp = icmp ult i32 %x_load, %y_load
  ret i1 %cmptmp
}

define i1 @MORE(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %cmptmp = icmp ugt i32 %x_load, %y_load
  ret i1 %cmptmp
}

define i1 @EQUALS(i32 %x, i32 %y) {
entry:
  %x_ptr = alloca i32, align 4
  %y_ptr = alloca i32, align 4
  store i32 %x, i32* %x_ptr, align 4
  store i32 %y, i32* %y_ptr, align 4
  %x_load = load i32, i32* %x_ptr, align 4
  %y_load = load i32, i32* %y_ptr, align 4
  %cmptmp = icmp eq i32 %x_load, %y_load
  ret i1 %cmptmp
}

define i32 @PRINT(i32 %this) {
entry:
  %calltemp = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @0, i32 0, i32 0), i32 %this)
  ret i32 %calltemp
}

define i32 @STR(i32 %this) {
entry:
  %this_ptr = alloca i32, align 4
  store i32 %this, i32* %this_ptr, align 4
  ret i32 0
}

define i32 @EQ(i32 %this) {
entry:
  %this_ptr = alloca i32, align 4
  store i32 %this, i32* %this_ptr, align 4
  ret i32 0
}

define i32 @calc_GPA(i32 %max, i32 %points, i32 %this) {
entry:
  %max_ptr = alloca i32, align 4
  store i32 %max, i32* %max_ptr, align 4
  %points_ptr = alloca i32, align 4
  store i32 %points, i32* %points_ptr, align 4
  %this_ptr = alloca i32, align 4
  store i32 %this, i32* %this_ptr, align 4
  %GPA = alloca i32, align 4
  store i32 0, i32* %GPA, align 4
  %score = alloca i32, align 4
  store i32 0, i32* %score, align 4
  %load_var_ptr = load i32, i32* %points_ptr, align 4
  %calltmp = call i32 @TIMES(i32 %load_var_ptr, i32 100)
  %load_var_ptr1 = load i32, i32* %max_ptr, align 4
  %calltmp2 = call i32 @DIVIDE(i32 %calltmp, i32 %load_var_ptr1)
  store i32 %calltmp2, i32* %score, align 4
  %load_var_arg = load i32, i32* %score, align 4
  %calltmp3 = call i1 @ATMOST(i32 %load_var_arg, i32 50)
  %if_cond = icmp eq i1 %calltmp3, true
  br i1 %if_cond, label %if_body, label %elif_inter

if_body:                                          ; preds = %entry
  store i32 1, i32* %GPA, align 4
  br label %if_cont

elif_inter:                                       ; preds = %entry
  %load_var_arg4 = load i32, i32* %score, align 4
  %calltmp5 = call i1 @ATMOST(i32 %load_var_arg4, i32 60)
  %elif_cond = icmp eq i1 %calltmp5, true
  br i1 %elif_cond, label %elif_body, label %elif_inter6

elif_body:                                        ; preds = %elif_inter
  store i32 2, i32* %GPA, align 4
  br label %if_cont

elif_inter6:                                      ; preds = %elif_inter
  %load_var_arg7 = load i32, i32* %score, align 4
  %calltmp8 = call i1 @ATMOST(i32 %load_var_arg7, i32 70)
  %elif_cond9 = icmp eq i1 %calltmp8, true
  br i1 %elif_cond9, label %elif_body10, label %elif_inter11

elif_body10:                                      ; preds = %elif_inter6
  store i32 3, i32* %GPA, align 4
  br label %if_cont

elif_inter11:                                     ; preds = %elif_inter6
  %load_var_arg12 = load i32, i32* %score, align 4
  %calltmp13 = call i1 @ATMOST(i32 %load_var_arg12, i32 80)
  %elif_cond14 = icmp eq i1 %calltmp13, true
  br i1 %elif_cond14, label %elif_body15, label %else_body

elif_body15:                                      ; preds = %elif_inter11
  store i32 4, i32* %GPA, align 4
  br label %if_cont

else_body:                                        ; preds = %elif_inter11
  store i32 5, i32* %GPA, align 4
  br label %if_cont

if_cont:                                          ; preds = %else_body, %elif_body15, %elif_body10, %elif_body, %if_body
  %rv_load = load i32, i32* %GPA, align 4
  ret i32 %rv_load
  ret i32 0
}

define i32 @PRINT.1(i32 %this) {
entry:
  %calltemp = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @1, i32 0, i32 0), i32 %this)
  ret i32 %calltemp
}

define i32 @STR.2(i32 %this) {
entry:
  %this_ptr = alloca i32, align 4
  store i32 %this, i32* %this_ptr, align 4
  ret i32 0
}

define i32 @EQ.3(i32 %this) {
entry:
  %this_ptr = alloca i32, align 4
  store i32 %this, i32* %this_ptr, align 4
  ret i32 0
}

define i32 @main() {
entry:
  %x = alloca i32, align 4
  store i32 99, i32* %x, align 4
  br label %loop_inter

loop_inter:                                       ; preds = %loop, %entry
  %load_var_arg = load i32, i32* %x, align 4
  %calltmp = call i1 @ATLEAST(i32 %load_var_arg, i32 50)
  %loop_cond = icmp eq i1 %calltmp, true
  br i1 %loop_cond, label %loop, label %afterloop

loop:                                             ; preds = %loop_inter
  %load_var_arg1 = load i32, i32* %x, align 4
  %calltmp2 = call i32 @calc_GPA(i32 100, i32 %load_var_arg1, i32 0)
  %calltmp3 = call i32 @PRINT(i32 %calltmp2)
  %load_var_arg4 = load i32, i32* %x, align 4
  %calltmp5 = call i32 @MINUS(i32 %load_var_arg4, i32 10)
  store i32 %calltmp5, i32* %x, align 4
  br label %loop_inter

afterloop:                                        ; preds = %loop_inter
  ret i32 0
}
